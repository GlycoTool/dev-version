# GlycanBuilder2

## Usage 1

1. unzip GlycanBuilder2.zip

2. Double-click `GlycanBuilder2.app`


## Usage 2

1. unzip GlycanBuilder2.zip

2. `cd ~/GlycanBuilder2`

3. Double-click `run.command`

## Troubleshooting
If you get a message saying "cannot verify that this app is free from malware", then you can perform the following:
1. Go to System Preferences ➙ Security & Privacy ➙ General
2. Click the lock in the lower right corner of the window
3. Enter your username and password, when prompted, and click Unlock
4. Click the App Store and Identified Developers radial button
5. Look for “GlycanBuilder2 was blocked from opening because it’s not from an identified developer” and click Open Anyway

You may get an error the first time you try to run the application, but if you try a second time, it should work.
The first time, there is some config file that needs to be set up, which occurs that first time.
So the second time, it will be ready to go.
