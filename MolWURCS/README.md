# MolWURCS dev version


# install

* https://gitlab.com/glycoinfo/molwurcs

## conditions

```
[glyconavi@calc ~]$ cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
[glyconavi@calc ~]$ java -version
openjdk version "1.8.0_302"
OpenJDK Runtime Environment (build 1.8.0_302-b08)
OpenJDK 64-Bit Server VM (build 25.302-b08, mixed mode)
[glyconavi@calc ~]$ mvn --version
Apache Maven 3.8.3 (ff8e977a158738155dc465c6a97ffaf31982d739)
Maven home: /opt/apache-maven
Java version: 1.8.0_302, vendor: Red Hat, Inc., runtime: /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.302.b08-0.el7_9.x86_64/jre
Default locale: ja_JP, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-1160.el7.x86_64", arch: "amd64", family: "unix"
```


```
$ git clone https://gitlab.com/glycoinfo/molwurcs.git
$ cd molwurcs
$ git checkout develop
$ mvn clean compile assembly:single

```

* MolWURCS version and help

```
[glyconavi@calc target]$ java -jar MolWURCS.jar -h                                                                           
usage: MolWURCS  [--with-aglycone] [--title-property-id <PROPERTY_ID>]
                --in <FORMAT> --out <FORMAT> [--output-no-result]
                [--double-check][<INPUT FORMAT STRING>]

Options:
    --double-check
 Double-check output WURCS using WURCS to WURCS convert, not active when
 "wurcs" is set to input and output formats at the same time
 -h,--help
 Show usage help
 -i,--in <FORMAT=[wurcs|mdlv2000|mdlv3000|sdf|mol2|pdb|cml|xyz|smi|inchi]>
 Set input format, required
 -n,--output-no-result
 Output result explicitly even if the conversion is failed or not
 contained result
 -o,--out <FORMAT=[wurcs|mdlv2000|sdf|mol2|pdb|cml|xyz|smi]>
 Set output format, required
 -p,--title-property-id <PROPERTY_ID>
 Use property value as title, which key of the value is <PROPERTY_ID>
    --with-aglycone
 Output WURCS with aglycone, active only when "wurcs" is specified as
 output format

Version: 0.7.4
```

## Repository

https://gitlab.com/glycoinfo/molwurcs

ae534d4b7cee7bffbd919e42185897db4162accc



## Usage 

```
$ cat CID5958.sdf | java -jar ./MolWURCS.jar --double-check -n -i sdf -o wurcs -p PUBCHEM_COMPOUND_CID 2>/dev/null
```

* result
```
5958	WURCS=2.0/1,1,0/[a2122h-1x_1-5_6*OPO/3O/3=O]/1/
```



## Usage 1
```
$ gzcat input.sdf.gz | java -jar MolWURCS.jar -i sdf -o wurcs
```

## Usage 2
```
$ gzcat input.sdf.gz | java -jar MolWURCS.jar -i sdf -o wurcs 1> ./result/std.out.log 2> ./result/std.err.log 
```

## Help
```
$ java -jar MolWURCS.jar -h
```





